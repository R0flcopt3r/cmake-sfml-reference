#pragma once
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Texture.hpp>

namespace Game {
class Entity : public sf::Sprite {
protected:
  int health;
  float speed;

public:
  Entity(const sf::Texture &texture, int posx = 0, int posy = 0);
};
} // namespace Game
