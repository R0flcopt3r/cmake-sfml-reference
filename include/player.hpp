#pragma once
#include "../include/entity.hpp"
#include <SFML/Graphics/Texture.hpp>

namespace Game {
class Player : public Game::Entity {
public:
  Player(const sf::Texture &texture, int posx = 0, int posy = 0);
  sf::Vector2<float> input_interface();
};
} // namespace Game
