#include "entity.hpp"
#include <SFML/Graphics/Sprite.hpp>

using namespace Game;

Entity::Entity(const sf::Texture &texture, int posx, int posy) : sf::Sprite(texture) {
  this->health = 100;
  this->speed = 500;
  this->setPosition(posx, posy);
}
