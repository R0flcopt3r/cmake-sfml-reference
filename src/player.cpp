#include "player.hpp"
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/System/Err.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <bits/types/struct_FILE.h>
#include <iostream>

using namespace Game;

Player::Player(const sf::Texture &texture, int posx, int posy)
    : Entity(texture, posx, posy) {}

sf::Vector2<float> Player::input_interface() {
  sf::Vector2<float> direction;
  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
    direction = sf::Vector2<float>(-this->speed, 0);
  } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
    direction = sf::Vector2<float>(this->speed, 0);
  } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
    direction = sf::Vector2<float>(0, -this->speed);
  } else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
    direction = sf::Vector2<float>(0, this->speed);
  }

  return direction;
}
