#include "player.hpp"
#include <SFML/Graphics.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/System/Err.hpp>
#include <SFML/System/Sleep.hpp>
#include <SFML/System/Time.hpp>
#include <SFML/System/Vector2.hpp>
#include <SFML/Window/VideoMode.hpp>
#include <SFML/Window/Window.hpp>
#include <algorithm>
#include <future>
#include <iostream>
#include <memory>
#include <vector>

void logg_player(const std::shared_ptr<Game::Player> player,
                 const sf::Window &window) {
  while (window.isOpen()) {
    std::cerr << "player pos x: " << player->getPosition().x
              << " x: " << player->getPosition().x << std::endl;
    sf::sleep(sf::milliseconds(500));
  }
}

void collisions(const std::shared_ptr<Game::Player> player,
                const std::vector<std::shared_ptr<Game::Player>> &entites,
                const sf::Window &window) {
  sf::Clock elapsed;
  auto old_position = player->getPosition();
  while (window.isOpen()) {
    int delta = elapsed.restart().asMilliseconds();
    for (const auto &entity : entites) {
      if (entity->getGlobalBounds().intersects(player->getGlobalBounds())) {
        player->setPosition(old_position);
      } else {
        old_position = player->getPosition();
      }
    }
    sf::sleep(sf::milliseconds(10 - delta));
  }
}

int main() {

  sf::RenderWindow window(sf::VideoMode(800, 600), "Gaymtezt");

  std::vector<std::shared_ptr<Game::Player>> entites;

  sf::Texture player_texture;
  if (!player_texture.loadFromFile("assets/char_1.png")) {
    std::cerr << "Error reading char_1.png" << std::endl;
    return 1;
  }
  sf::Texture thing_texture;
  if (!thing_texture.loadFromFile("assets/char_2.png")) {
    std::cerr << "Error reading char_2.png" << std::endl;
    return 1;
  }

  auto player = std::make_shared<Game::Player>(player_texture);

  entites.push_back(
      std::make_shared<Game::Player>(Game::Player(thing_texture, 500, 500)));

  sf::Clock elapsed;

  auto log_future =
      std::async(std::launch::async, logg_player, player, std::ref(window));

  auto collision_future = std::async(std::launch::async, collisions, player,
                                     std::ref(entites), std::ref(window));

  while (window.isOpen()) {
    float delta_time = elapsed.restart().asSeconds();

    sf::Event event;
    while (window.pollEvent(event)) {
      if (event.type == sf::Event::Closed)
        window.close();
    }

    window.clear();
    window.draw(*player);
    for (auto &entity : entites) {
      window.draw(*entity);
    }

    sf::Vector2<float> direction = player->input_interface();
    sf::Vector2<float> old_position = player->getPosition();

    // Collision detection
    player->move(direction * delta_time);
    // for (auto &entity : entites) {
    //   if (entity->getGlobalBounds().intersects(player->getGlobalBounds())) {
    //     player->setPosition(old_position);
    //   }
    // }
    sf::sleep(sf::milliseconds(16));
    window.display();
  }

  return 0;
}
